class shiftRegister{//Класс для работы со сдвиговым регистром
  private:
    bool shR[8]={false};//Выход сигнала с кадой ноги
    void outData(){//Вывод данных на ноги
      int sum = 0;
      int intager[8] = {1,2,4,8,16,32,64,128};
      for(int i = 0; i<8; i++){
        if(shR[i])sum+=intager[i];
      }
      digitalWrite(pinLetch, LOW);
      shiftOut(pinData, pinClock, MSBFIRST, sum);  
      digitalWrite(pinLetch, HIGH);
    }
  public:
  int pinData;
  int pinLetch;
  int pinClock;

  //Конструктор класса
  shiftRegister(int pinData, int pinLetch, int pinClock){
    this->pinData = pinData;
    this->pinLetch = pinLetch;
    this->pinClock = pinClock;
    pinMode(this->pinData, OUTPUT);
    pinMode(this->pinLetch, OUTPUT);
    pinMode(this->pinClock, OUTPUT);
  }

  //Изменение сигнала на true или false
  void Write(int pin, bool t_f){//  int pin = 0-7
      this->shR[pin] = t_f;
      this->outData();//Вывод данных
  }

  //Все ноки на false
  void Clear(){
    for(int i = 0; i<8; i++)
      this->shR[i]=false;
    this->outData();//Вывод данных
  }
};






shiftRegister number(4, 3, 2);//Создаем обЪект класса

void setup() {
  Serial.begin(9600);
}
void loop() {

  
  char ch;
  if(Serial.available() > 0){//Поступило ли что-нибудь на порт 
    ch = Serial.read();//Считываем с порта
      number.Clear();//Очищаем семисегментный индикатор/переводим все ноки в полоение false
    switch(ch){
      case '0':
       number.Write(0, true);//Подаем сигнал на определенные ноки
       number.Write(1, true);
       number.Write(2, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '1':
       number.Write(2, true);
       number.Write(5, true);
      break;
      case '2':
       number.Write(0, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
      case '3':
       number.Write(0, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '4':
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(5, true);
      break;
      case '5':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '6':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '7':
       number.Write(0, true);
       number.Write(2, true);
       number.Write(5, true);
      break;
      case '8':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '9':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case '.':
       number.Write(7, true);
      break;
      case 'a':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
      break;
      case 'b':
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'c':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
      case 'd':
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'e':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
      case 'f':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
      break;
      case 'g':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'h':
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
      break;
      case 'i':
       number.Write(1, true);
       number.Write(4, true);
      break;
      case 'j':
       number.Write(2, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'l':
       number.Write(1, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
      case 'o':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'p':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
      break;
      case 'q':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(5, true);
      break;
      case 'r':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(4, true);
      break;
      case 's':
       number.Write(0, true);
       number.Write(1, true);
       number.Write(3, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 't':
       number.Write(1, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
      case 'u':
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'v':
       number.Write(1, true);
       number.Write(2, true);
       number.Write(4, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'x':
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(5, true);
      break;
      case 'y':
       number.Write(1, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(5, true);
       number.Write(6, true);
      break;
      case 'z':
       number.Write(0, true);
       number.Write(2, true);
       number.Write(3, true);
       number.Write(4, true);
       number.Write(6, true);
      break;
    }
    delay(500);
  }
}

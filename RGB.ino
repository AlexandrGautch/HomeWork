#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11); // RX, TX

#define RED 3
#define GREEN 5
#define BLUE 6

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  mySerial.begin(9600);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}
String str = "", str2 = "";
char ch, ch2;
bool t_f=false;
void loop() {
  if(Serial.available()){
    t_f=true;
      mySerial.write(Serial.read());  
  }
  else if(t_f){
    t_f=false;
      mySerial.write('!'); 
  }
  if(mySerial.available()){
      ch2=mySerial.read();
      if(ch2=='!'){
        //Serial.println(str);
        make_color(str2);
        str2="";
      }
      else if(ch2=='?'){
          Serial.println(str2);
          str2="";
      }
      else{
        str2+=ch2;  
      }
  }
}

void make_color(String color){
  if(color == "red")
      make_color(255, 0, 0);
  else if(color == "green")
      make_color(0, 255, 0);
  else if(color == "blue")
      make_color(0, 0, 255);
  else if(color == "purple")
      make_color(255, 0, 255);
  else if(color == "yellow")
      make_color(255, 255, 0);
  else if(color == "aqua")
      make_color(0, 255, 255);
  else if(color == "white")
      make_color(255, 255, 255);
  else mySerial.write(" Color invalid? Valid color:? \tred? \tgreen? \tblue? \tyellow? \tpurple? \taqua? \twhite?");
}

void make_color(int red, int green, int blue){
      analogWrite(RED, map(red, 0, 255, 0, 1023));
      analogWrite(GREEN, map(green, 0, 255, 0, 1023));
      analogWrite(BLUE, map(blue, 0, 255, 0, 1023));
}
